TEMPLATE = subdirs

KEY_VALUE_REPO = ViaKeyValueRepo
HTTP_SERVER = 3rdParty/QtHttpServer

SUBDIRS = $${KEY_VALUE_REPO} \
          $${HTTP_SERVER}

$${KEY_VALUE_REPO}.depends = $${HTTP_SERVER}
