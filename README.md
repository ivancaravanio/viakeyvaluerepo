# Via Key Value Service
## Implementation remarks
1/ Implementation
---
The project is implemented using:
- C++17
- [Qt](https://www.qt.io/) framework
- [QtHttpServer](https://www.qt.io/blog/2019/01/25/introducing-qt-http-server) ([2](https://www.qt.io/blog/2019/02/01/qhttpserver-routing-api)) - current version: [Qt Extensions](https://code.qt.io/cgit/qt-extensions/qthttpserver.git/), previous version: [Qt Labs](https://code.qt.io/cgit/qt-labs/qthttpserver.git/). Since the Qt framework doesn't have an integrated HTTP server but only a thorough [networking module](https://doc.qt.io/qt-6/qtnetwork-module.html) thus either a manual implementation was needed or incorporation of a ready-made third party solution. The QtHttpServer project is not part of the official Qt framework but has been implemented as an experimental project e.g. Qt Labs' one and has matured to a potentially official such - Qt Extensions' one. Other C++-based and potentially Qt-enabled HTTP server implementations options were: [Cutelyst](https://cutelyst.org/) and [Served](https://github.com/meltwater/served) where only the first one was Qt-based. It was cmake-based as well which made it not directly incorporatable as a sub-project and Served was not Qt-based and was mandating Boost. Thus QtHttpServer was chosen as the simplest and most integrated way to go - both Qt and [qmake](https://doc.qt.io/qt-5/qmake-manual.html)-based.

2/ QtHttpServer
---
There is an issue with QtHttpServer, though - it [doesn't support queries natively](https://www.qt.io/blog/2019/02/01/qhttpserver-routing-api):
> By default, QHttpServer::router works only with a path from a url. If you want to create a specific rule that works with a query,
you can inherit from QHttpServerRouterRule and pass it as a template argument to QHttpServer::route.

There is no documentation how to do the aforemention extension and lurking inside the QtHttpServer's implementation to find places where `QHttpServerRouterRule` was either used directly or inherited did provide some clues but more time is needed for deeper insights and thus making correct use of it. As a temporary solution, which I apologize for, I have reformatted the queries to paths:
`/set?k={k}&v={v}` => `/set/k={k}/v={v}`

3/ Database
---
> Use a database for storing the keys and values. Could be CockroachDB or MongoDB.

I wanted to incorporate MongoDB despite that Qt [doesn't provide ready-made integration with this DBMS](https://doc-snapshots.qt.io/qt6-dev/sql-driver.html). But I saw its build needed a lot of setting up - Boost as well as a separate build for its C-based driver - [MongoDB C](https://docs.mongodb.com/drivers/c/). Also saw several topics where users have tried to build it but have stumbled upon various build issues. It was at the very end of the project's implementation where I found that there is an [installable package](https://mongodb-documentation.readthedocs.io/en/latest/ecosystem/tutorial/getting-started-with-cpp-driver.html#windows). Thus went for the integrated approach - used Qt's integrated SQLite support. Again apologies here for not matching with the requirements but still wanted to provide a finalized and complete solution as a first priority and not spent considerable amount of time with setting up the DBMS.
One additional note here - the server can be configured to use either an in-memory database - currently a simple hash associative container or the persisted DBMS storage.

4/ Performance
---
> Try to implement the above requests for very big stores
What will be the performance for millions of keys and values?

I wanted but I haven't incorporated any explicit performance optimizations since I wanted to have the complete working implementation first. For sure multiple threads could be employed and thus multiple database connections. By multiple threads - a thread-pool could be used in conjunction with a database connections pool. [SQLite supports multi-threading](https://sqlite.org/threadsafe.html) but still it is an option to switch to another DBMS for the purpose.

5/ Deployment
---
> NB: Provide docker-compose or another single-command to start the solution

Will provide you in an hour or so with the built version that would require a single line to be launched. The best would have been to upload it to a publicly reachable HTTP server but despite the extended time for implementation was still short on it for this aspect.

6/ Task 4 (hard) - Distributed KV Store
---
> Task 4 (hard) - Distributed KV Store

Would definitely need more time to research for the potential solutions and implement this task.

7/ HTTP verbs
---
> Implement Key-Value store with web-based API with the following end-points (for simplicity use HTTP GET for all endpoints):

Both PUT and GET HTTP verbs are made allowed for the modifying requests.


## Tasks

### Task 1
Implement Key-Value store with web-based API with the following end-points (for simplicity use HTTP GET for all endpoints):

1. `/set?k={k}&v={v}`
    - Set key `k` with value `v`
    - `k` is a string not bigger than 64 chars
    - `v` is a string not bigger than 256 chars
1. `/get?k={k}`
    - Gets value with key `k`
    - Should return 404 if the key is missing
1. `/rm?k={k}`
    - Removes key `k`
    - If the key is missing should return an error
1. `/clear`
    - Removes all keys and values
1. `/is?k={k}`
    - Returns HTTP 200 if the key exists
    - Returns HTTP 404 if the key is missing 

### Tasks 2

1. `/getKeys` 
    - Should return all the keys in the store
2. `/getValues`
    - Should return all the values in the store
3. `/getAll`
    - Should return all pairs of key and value
4. Try to implement the above requests for very big stores
    - What will be the performance for millions of keys and values?

### Task 3
1. Use a database for storing the keys and values. Could be CockroachDB or MongoDB. 
*NB: Provide docker-compose or another single-command to start the solution*

### Task 4 (hard) - Distributed KV Store
Please, try to implement the following task, or research how to implement and suggest architectual design.<br/>

For the following task the store should be in-memory per service. No external storage is allowed.
1. Implement a mesh/cluster of KV services that are consistant and everyone has all the keys and values all the time
    - Implement a new end-point for the server to join a mesh of servers.
    - Implement a feature of removing a node from the mesh
2. How you are going solve merge conflicts in distributed transaction?
    - Propose a solution
3. Implement multi active behavior for all nodes in the cluster.
    - Having a load balancer in front of the cluster should work as expected.
4. Implement stress test and testing service

## Acceptable languages
Go, Node.js, Java, Python, C++.
Preferred language: Go

## Criteria
The following are taken into consideration
 * Clean code
 * Proper comments in code (quality, not quantity)
 * Architecture
 * Documentation
 * Tests
 * Linters, CI configuration, .gitignore, test coverage reporting, etc are a bonus
 * Single command to start the solution
 * Single commands and/or documentation for other functionality (e.g. running tests, running linter)

 ## License
 The work you do is copyrighted by Via Engineering Bulgaria. [License notice](./LICENSE)
 
 
