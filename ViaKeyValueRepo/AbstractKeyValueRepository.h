#ifndef ABSTRACTKEYVALUEREPOSITORY_H
#define ABSTRACTKEYVALUEREPOSITORY_H

#include <QHash>
#include <QString>
#include <QStringList>

class AbstractKeyValueRepository
{
public:
    AbstractKeyValueRepository();
    virtual ~AbstractKeyValueRepository();
    Q_DISABLE_COPY( AbstractKeyValueRepository )

    static bool isValid( const QString& entity, const bool isValue );

    virtual bool                      set( const QString& key, const QString& value );
    virtual bool                      remove( const QString& key );
    virtual void                      clear()                                         = 0;
    virtual QString                   value( const QString& key ) const;
    virtual QStringList               keys() const                                    = 0;
    virtual QStringList               values() const                                  = 0;
    virtual QHash< QString, QString > keyValuePairs() const                           = 0;
};

#endif // ABSTRACTKEYVALUEREPOSITORY_H
