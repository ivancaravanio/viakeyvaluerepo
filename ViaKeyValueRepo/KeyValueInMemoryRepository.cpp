#include "KeyValueInMemoryRepository.h"

KeyValueInMemoryRepository::~KeyValueInMemoryRepository()
{
}

bool KeyValueInMemoryRepository::set( const QString& key, const QString& value )
{
    if ( !this->AbstractKeyValueRepository::set( key, value ) )
    {
        return false;
    }

    _keyValueRepository.insert( key, value );
    return true;
}

bool KeyValueInMemoryRepository::remove( const QString& key )
{
    return this->AbstractKeyValueRepository::remove( key ) && _keyValueRepository.remove( key );
}

void KeyValueInMemoryRepository::clear()
{
    _keyValueRepository.clear();
}

QString KeyValueInMemoryRepository::value( const QString& key ) const
{
    const QString validator = this->AbstractKeyValueRepository::value( key );
    return validator.isNull() ? validator : _keyValueRepository.value( key );
}

QStringList KeyValueInMemoryRepository::keys() const
{
    return _keyValueRepository.keys();
}

QStringList KeyValueInMemoryRepository::values() const
{
    return _keyValueRepository.values();
}

QHash< QString, QString > KeyValueInMemoryRepository::keyValuePairs() const
{
    return _keyValueRepository;
}
