#include "HttpServer.h"

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

const QHostAddress HttpServer::s_defaultHost{ QHostAddress::Any };
const quint16      HttpServer::s_defaultPort = 80u;
const quint16      HttpServer::s_anyPort     = 0u;

HttpServer::HttpServer( AbstractKeyValueRepository* const aKeyValueRepository,
                        const QHostAddress&               aHost,
                        const quint16                     aPort )
    : _keyValueRepository{ aKeyValueRepository }
{
    // https://www.qt.io/blog/2019/02/01/qhttpserver-routing-api
    // alternatives:
    // 1. https://cutelyst.org/
    // 2. https://github.com/meltwater/served
    /*  
     * Task 1
     * __  
     *     /set?k={k}&v={v}
     *         Set key k with value v
     *         k is a string not bigger than 64 chars
     *         v is a string not bigger than 256 chars
     *     /get?k={k}
     *         Gets value with key k
     *         Should return 404 if the key is missing
     *     /rm?k={k}
     *         Removes key k
     *         If the key is missing should return an error
     *     /clear
     *         Removes all keys and values
     *     /is?k={k}
     *         Returns HTTP 200 if the key exists
     *         Returns HTTP 404 if the key is missing
     * Tasks 2
     * __
     *     /getKeys
     *         Should return all the keys in the store
     *     /getValues
     *         Should return all the values in the store
     *     /getAll
     *         Should return all pairs of key and value
     *     Try to implement the above requests for very big stores
     *         What will be the performance for millions of keys and values?
     */

    const QHttpServerRequest::Methods writerMethodTypes = QHttpServerRequest::Methods{
        QHttpServerRequest::Method::Get
    } | QHttpServerRequest::Method::Put;

    _httpServer.route( "/set/k=<arg>/v=<arg>",
                       writerMethodTypes,
                       [ this ]( const QString& aKey, const QString& aValue ) {
                           return this->_keyValueRepository->set( aKey, aValue )
                                      ? QHttpServerResponder::StatusCode::Ok
                                      : QHttpServerResponder::StatusCode::BadRequest;
                       } );
    _httpServer.route( "/get/k=",
                       QHttpServerRequest::Method::Get,
                       [ this ]( const QString& aKey, QHttpServerResponder&& aResponder ) {
                           const QString value = this->_keyValueRepository->value( aKey );
                           if ( value.isNull() )
                           {
                               aResponder.write( QHttpServerResponder::StatusCode::NotFound );
                           }
                           else
                           {
                               aResponder.write( value.toLatin1(),
                                                 "text/plain",
                                                 QHttpServerResponder::StatusCode::Ok );
                           }
                       } );
    _httpServer.route( "/rm/k=", writerMethodTypes, [ this ]( const QString& aKey ) {
        return this->_keyValueRepository->remove( aKey )
                   ? QHttpServerResponder::StatusCode::Ok
                   : QHttpServerResponder::StatusCode::NotFound;
    } );
    _httpServer.route( "/clear", writerMethodTypes, [ this ] {
        this->_keyValueRepository->clear();
        return QHttpServerResponder::StatusCode::Ok;
    } );
    _httpServer.route( "/is/k=", QHttpServerRequest::Method::Get, [ this ]( const QString& aKey ) {
        return this->_keyValueRepository->value( aKey ).isNull()
                   ? QHttpServerResponder::StatusCode::NotFound
                   : QHttpServerResponder::StatusCode::Ok;
    } );
    _httpServer.route( "/getKeys",
                       QHttpServerRequest::Method::Get,
                       [ this ]( QHttpServerResponder&& aResponder ) {
                           aResponder.write( QJsonDocument{
                               QJsonArray::fromStringList( this->_keyValueRepository->keys() ) } );
                       } );
    _httpServer.route( "/getValues",
                       QHttpServerRequest::Method::Get,
                       [ this ]( QHttpServerResponder&& aResponder ) {
                           aResponder.write( QJsonDocument{ QJsonArray::fromStringList(
                               this->_keyValueRepository->values() ) } );
                       } );
    _httpServer.route(
        "/getAll", QHttpServerRequest::Method::Get, [ this ]( QHttpServerResponder&& aResponder ) {
            QJsonObject jsonMap;

            const QHash< QString, QString > keyValueMappings = this->_keyValueRepository
                                                                   ->keyValuePairs();
            const QHash< QString, QString >::const_iterator iterEnd = keyValueMappings.constEnd();
            for ( QHash< QString, QString >::const_iterator iter = keyValueMappings.constBegin();
                  iter != iterEnd;
                  ++iter )
            {
                jsonMap[ iter.key() ] = iter.value();
            }

            aResponder.write( QJsonDocument{ jsonMap } );
        } );

    _httpServer.listen( aHost, aPort );
}
