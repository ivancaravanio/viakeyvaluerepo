QT -= gui
QT += sql network

CONFIG *= console c++latest precompile_header
CONFIG -= app_bundle

SOURCES += \
    AbstractKeyValueRepository.cpp \
    HttpServer.cpp \
    KeyValueInMemoryRepository.cpp \
    KeyValuePersistedRepository.cpp \
    KeyValueRepositoryConfig.cpp \
    KeyValueRepositoryFactory.cpp \
    main.cpp

HEADERS += \
    AbstractKeyValueRepository.h \
    HttpServer.h \
    KeyValueInMemoryRepository.h \
    KeyValuePersistedRepository.h \
    KeyValueRepositoryConfig.h \
    KeyValueRepositoryFactory.h \
    ViaKeyValueRepositoryAllHeaders.h

PRECOMPILED_HEADER = ViaKeyValueRepositoryAllHeaders.h

VER_MAJ = 1
VER_MIN = 0
VER_PAT = 0
VERSION = $$join($$list($${VER_MAJ} $${VER_MIN} $${VER_PAT}), .)

# https://stackoverflow.com/questions/2784697/setting-application-info-in-a-qt-executable-file-on-windows
# -> https://stackoverflow.com/a/11971210/796109

QMAKE_TARGET_COMPANY = Via Transportation, Inc. & Co.
QMAKE_TARGET_PRODUCT = Via Key-Value repository HTTP server
QMAKE_TARGET_DESCRIPTION = A key-value store - both in-memory and using DMBS, as well as an HTTP server.
QMAKE_TARGET_COPYRIGHT = Via Transportation, Inc. & Co.

RC_ICONS = Resources/Via.ico

defineTest(isDebugBuild) {
    CONFIG(debug, debug|release) {
        return (true)
    } else {
        return (false)
    }
}

defineReplace(buildDirName) {
    isDebugBuild() {
        return (debug)
    } else {
        return (release)
    }
}

defineReplace(buildDecoratedBinaryName) {
    win32 {
        isDebugBuild() {
            return ($${1}d)
        }
    }

    return ($${1})
}

QT_HTTP_SERVER_DIR_PATH = $${OUT_PWD}/../3rdParty/QtHttpServer
QT_HTTP_SERVER_BINARIES_DIR_PATH = $${QT_HTTP_SERVER_DIR_PATH}/lib
QT_HTTP_SERVER_BINARY_FILE_BASE_NAME = $$buildDecoratedBinaryName( Qt$${QT_MAJOR_VERSION}HttpServer )
QT_SLL_SERVER_BINARY_FILE_BASE_NAME = $$buildDecoratedBinaryName( Qt$${QT_MAJOR_VERSION}SslServer )

INCLUDEPATH += $${QT_HTTP_SERVER_DIR_PATH}/include
LIBS += -L$${QT_HTTP_SERVER_BINARIES_DIR_PATH} \
        -l$${QT_HTTP_SERVER_BINARY_FILE_BASE_NAME} \
        -l$${QT_SLL_SERVER_BINARY_FILE_BASE_NAME}

OUTPUT_DIR_PATH = $${OUT_PWD}/$$buildDirName()

for ( BINARY_FILE_BASE_NAME, $$list($${QT_HTTP_SERVER_BINARY_FILE_BASE_NAME} $${QT_SLL_SERVER_BINARY_FILE_BASE_NAME}) ) {
    $${BINARY_FILE_BASE_NAME}.files = $${QT_HTTP_SERVER_BINARIES_DIR_PATH}/$${BINARY_FILE_BASE_NAME}.$${QMAKE_EXTENSION_SHLIB}
    $${BINARY_FILE_BASE_NAME}.path = $${OUTPUT_DIR_PATH}
    INSTALLS += $${BINARY_FILE_BASE_NAME}
}
