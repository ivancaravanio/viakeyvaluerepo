#include "KeyValuePersistedRepository.h"

#include <QCoreApplication>
#include <QDir>
#include <QSqlError>
#include <QSqlQuery>
#include <QStringBuilder>
#include <QVariant>

// #define DB_USES_USER_APP_DATA_FOLDER
#ifdef DB_USES_USER_APP_DATA_FOLDER
#    include <QStandardPaths>
#endif

KeyValuePersistedRepository::KeyValuePersistedRepository()
{
    this->createAndConnectToDatabase();
}

KeyValuePersistedRepository::~KeyValuePersistedRepository()
{
}

void KeyValuePersistedRepository::createAndConnectToDatabase()
{
    if ( _databaseConnection.isOpen() )
    {
        return;
    }

    if ( !_databaseConnection.isValid() )
    {
        _databaseConnection = KeyValuePersistedRepository::createDatabaseConnection();
        if ( !_databaseConnection.isValid() )
        {
            return;
        }
    }

    if ( !_databaseConnection.open() )
    {
        _databaseConnection = QSqlDatabase();
    }
}

QSqlDatabase KeyValuePersistedRepository::createDatabaseConnection()
{
    QDir dbDir{
#ifdef DB_USES_USER_APP_DATA_FOLDER
        QStandardPaths::writableLocation( QStandardPaths::AppDataLocation )
#else
        QCoreApplication::applicationDirPath()
#endif
    };
    if ( !dbDir.exists() && !dbDir.mkpath( QString{ '.' } ) )
    {
        return {};
    }

    QSqlDatabase databaseConnection = QSqlDatabase::addDatabase( "QSQLITE" );
    databaseConnection.setDatabaseName( dbDir.filePath( qAppName() + ".db" ) );

    const bool databaseAlreadyExists = QFileInfo::exists( databaseConnection.databaseName() );
    if ( !databaseConnection.open() )
    {
        return {};
    }

    if ( databaseAlreadyExists )
    {
        return databaseConnection;
    }

    QSqlQuery schemeCreator{ databaseConnection };
    return schemeCreator.exec( "CREATE TABLE key_value_repo ("
                               "key TEXT NOT NULL PRIMARY KEY, "
                               "value TEXT NOT NULL)" )
               ? databaseConnection
               : QSqlDatabase{};
}

QStringList KeyValuePersistedRepository::entities( const bool     shouldProvideValues,
                                                   const QString& key ) const
{
    QSqlQuery entitiesProvider{ _databaseConnection };
    entitiesProvider.setForwardOnly( true );

    QString entitiesRawProvider = QString{ "SELECT %1 FROM key_value_repo" }.arg(
        shouldProvideValues ? "value" : "key" );
    const bool shouldFilter = !key.isNull();
    if ( shouldFilter )
    {
        entitiesRawProvider += " WHERE key = ?";
    }

    entitiesProvider.prepare( entitiesRawProvider );

    if ( shouldFilter )
    {
        entitiesProvider.addBindValue( key );
    }

    if ( !entitiesProvider.exec() )
    {
        return {};
    }

    QStringList entities;
    const int   keysCountGuess = entitiesProvider.size();
    if ( keysCountGuess >= 0 )
    {
        entities.reserve( keysCountGuess );
    }

    while ( entitiesProvider.next() )
    {
        entities.append( entitiesProvider.value( 0 ).toString() );
    }

    return entities;
}

bool KeyValuePersistedRepository::set( const QString& key, const QString& value )
{
    if ( !this->AbstractKeyValueRepository::set( key, value ) )
    {
        return false;
    }

    QSqlQuery addOrReplaceActor{ _databaseConnection };
    addOrReplaceActor.prepare( "INSERT OR REPLACE INTO key_value_repo (key, value) "
                               "VALUES (?, ?)" );
    addOrReplaceActor.addBindValue( key );
    addOrReplaceActor.addBindValue( value );
    return addOrReplaceActor.exec();
}

bool KeyValuePersistedRepository::remove( const QString& key )
{
    if ( !this->AbstractKeyValueRepository::remove( key ) )
    {
        return false;
    }

    QSqlQuery remover{ _databaseConnection };
    remover.prepare( "DELETE FROM key_value_repo "
                     "WHERE key = ?" );
    remover.addBindValue( key );
    return remover.exec();
}

void KeyValuePersistedRepository::clear()
{
    QSqlQuery wiper{ _databaseConnection };
    wiper.prepare( "DELETE FROM key_value_repo" );
    wiper.exec();
}

QString KeyValuePersistedRepository::value( const QString& key ) const
{
    if ( this->AbstractKeyValueRepository::value( key ).isNull() )
    {
        return {};
    }

    const QStringList entities = this->entities( true, key );
    return entities.isEmpty() ? QString{} : entities.first();
}

QStringList KeyValuePersistedRepository::keys() const
{
    return this->entities( false );
}

QStringList KeyValuePersistedRepository::values() const
{
    return this->entities( true );
}

QHash< QString, QString > KeyValuePersistedRepository::keyValuePairs() const
{
    QSqlQuery keyValuePairsProvider{ _databaseConnection };
    keyValuePairsProvider.setForwardOnly( true );
    keyValuePairsProvider.prepare( "SELECT key, value FROM key_value_repo" );

    if ( !keyValuePairsProvider.exec() )
    {
        return {};
    }

    QHash< QString, QString > keyValuePairs;
    const int                 keyValuePairsCountGuess = keyValuePairsProvider.size();
    if ( keyValuePairsCountGuess >= 0 )
    {
        keyValuePairs.reserve( keyValuePairsCountGuess );
    }

    while ( keyValuePairsProvider.next() )
    {
        keyValuePairs.insert( keyValuePairsProvider.value( 0 ).toString(),
                              keyValuePairsProvider.value( 1 ).toString() );
    }

    return keyValuePairs;
}
