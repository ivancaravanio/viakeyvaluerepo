#ifndef KEYVALUEREPOSITORYCONFIG_H
#define KEYVALUEREPOSITORYCONFIG_H

#include "KeyValueRepositoryFactory.h"

#include <QCoreApplication>
#include <QHostAddress>

#include <functional>
#include <optional>

class KeyValueRepositoryConfig
{
public:
    static KeyValueRepositoryConfig from( const QCoreApplication& aApp );

public:
    const std::optional< KeyValueRepositoryFactory::KeyValueRepositoryType > keyValueRepositoryType;
    const QHostAddress                                                       host;
    const std::optional< quint16 >                                           port;

private:
    static QString databaseTypeDescription(
        const KeyValueRepositoryFactory::KeyValueRepositoryType aKeyValueRepositoryType );
    static QString databaseTypeSpecifier(
        const KeyValueRepositoryFactory::KeyValueRepositoryType aKeyValueRepositoryType );
    static std::optional< KeyValueRepositoryFactory::KeyValueRepositoryType > databaseType(
        const KeyValueRepositoryFactory::KeyValueRepositoryType aKeyValueRepositoryType );

    static void forEachKeyValueRepositoryType(
        std::function< bool( const KeyValueRepositoryFactory::KeyValueRepositoryType ) >
            aKeyValueRepositoryTypeProcessor );
};

#endif // KEYVALUEREPOSITORYCONFIG_H
