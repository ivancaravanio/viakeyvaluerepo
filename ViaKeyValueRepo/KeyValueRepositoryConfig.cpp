#include "KeyValueRepositoryConfig.h"

#include "HttpServer.h"

#include <QCommandLineOption>
#include <QCommandLineParser>
#include <QTextStream>

KeyValueRepositoryConfig KeyValueRepositoryConfig::from( const QCoreApplication& aApp )
{
    QCommandLineParser       cmdLineParser;
    const QCommandLineOption hostCmdLineOption{ { "a", "address" },
                                                "The host address.",
                                                "host_address" };
    const QCommandLineOption portCmdLineOption{ { "p", "port" },
                                                "The TCP port number.",
                                                "port_number" };

    QString     databaseTypeDescription;
    QTextStream databaseTypeDescriptionWriter{ &databaseTypeDescription };
    databaseTypeDescriptionWriter << "Specifies the type of the database:";

    KeyValueRepositoryConfig::forEachKeyValueRepositoryType(
        [ &databaseTypeDescriptionWriter ](
            const KeyValueRepositoryFactory::KeyValueRepositoryType aKeyValueRepoType ) {
            databaseTypeDescriptionWriter
                << Qt::endl
                << "- " << KeyValueRepositoryConfig::databaseTypeDescription( aKeyValueRepoType ) << " ("
                << KeyValueRepositoryConfig::databaseTypeSpecifier( aKeyValueRepoType ) << ')';
            return true;
        } );

    const QCommandLineOption databaseTypeCmdLineOption{ { "d", "dbtype" },
                                                        databaseTypeDescription,
                                                        "database_type" };

    cmdLineParser.addOptions( { hostCmdLineOption, portCmdLineOption, databaseTypeCmdLineOption } );
    cmdLineParser.addHelpOption();
    cmdLineParser.addVersionOption();
    cmdLineParser.process( aApp );

    QHostAddress host;
    if ( cmdLineParser.isSet( hostCmdLineOption ) )
    {
        if ( !host.setAddress( cmdLineParser.value( hostCmdLineOption ) ) )
        {
            host.clear();
        }
    }

    std::optional< quint16 > port;
    if ( cmdLineParser.isSet( portCmdLineOption ) )
    {
        bool          isConverted         = false;
        const quint16 portNumberCandidate = cmdLineParser.value( portCmdLineOption )
                                                .toUShort( &isConverted );
        if ( isConverted )
        {
            port = portNumberCandidate;
        }
    }

    std::optional< KeyValueRepositoryFactory::KeyValueRepositoryType > keyValueRepositoryType;
    if ( cmdLineParser.isSet( databaseTypeCmdLineOption ) )
    {
        const QString keyValueRepositoryTypeRaw = cmdLineParser.value( databaseTypeCmdLineOption );
        KeyValueRepositoryConfig::forEachKeyValueRepositoryType(
            [ &keyValueRepositoryTypeRaw, &keyValueRepositoryType ](
                const KeyValueRepositoryFactory::KeyValueRepositoryType aKeyValueRepositoryType ) {
                if ( keyValueRepositoryTypeRaw
                     == KeyValueRepositoryConfig::databaseTypeSpecifier( aKeyValueRepositoryType ) )
                {
                    keyValueRepositoryType = aKeyValueRepositoryType;
                    return true;
                }

                return false;
            } );
    }

    return { keyValueRepositoryType, host, port };
}

QString KeyValueRepositoryConfig::databaseTypeDescription(
    const KeyValueRepositoryFactory::KeyValueRepositoryType aKeyValueRepoType )
{
    switch ( aKeyValueRepoType )
    {
        case KeyValueRepositoryFactory::KeyValueRepositoryType::InMemory:
            return "in-memory";

        case KeyValueRepositoryFactory::KeyValueRepositoryType::Persisted:
            return "persisted";
    }

    return {};
}

QString KeyValueRepositoryConfig::databaseTypeSpecifier(
    const KeyValueRepositoryFactory::KeyValueRepositoryType aKeyValueRepoType )
{
    switch ( aKeyValueRepoType )
    {
        case KeyValueRepositoryFactory::KeyValueRepositoryType::InMemory:
            return "inmemory";

        case KeyValueRepositoryFactory::KeyValueRepositoryType::Persisted:
            return "persisted";
    }

    return {};
}

void KeyValueRepositoryConfig::forEachKeyValueRepositoryType(
    std::function< bool( const KeyValueRepositoryFactory::KeyValueRepositoryType ) >
        aKeyValueRepositoryTypeProcessor )
{
    for ( const KeyValueRepositoryFactory::KeyValueRepositoryType keyValueRepositoryType :
          { KeyValueRepositoryFactory::KeyValueRepositoryType::InMemory,
            KeyValueRepositoryFactory::KeyValueRepositoryType::Persisted } )
    {
        if ( !aKeyValueRepositoryTypeProcessor( keyValueRepositoryType ) )
        {
            return;
        }
    }
}
