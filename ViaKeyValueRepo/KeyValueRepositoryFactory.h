#ifndef KEYVALUEREPOSITORYFACTORY_H
#define KEYVALUEREPOSITORYFACTORY_H

#include <qcompilerdetection.h>

class AbstractKeyValueRepository;

class KeyValueRepositoryFactory
{
public:
    enum class KeyValueRepositoryType : int
    {
        InMemory,
        Persisted
    };

public:
    KeyValueRepositoryFactory() Q_DECL_EQ_DELETE;
    ~KeyValueRepositoryFactory() Q_DECL_EQ_DELETE;
    Q_DISABLE_COPY_MOVE( KeyValueRepositoryFactory );

    static AbstractKeyValueRepository* create( const KeyValueRepositoryType aType );
};

#endif // KEYVALUEREPOSITORYFACTORY_H
