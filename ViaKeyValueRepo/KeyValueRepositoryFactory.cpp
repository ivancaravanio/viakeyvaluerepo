#include "KeyValueRepositoryFactory.h"

#include "KeyValueInMemoryRepository.h"
#include "KeyValuePersistedRepository.h"

AbstractKeyValueRepository* KeyValueRepositoryFactory::create(
    const KeyValueRepositoryFactory::KeyValueRepositoryType aType )
{
    switch ( aType )
    {
        case KeyValueRepositoryType::InMemory:
            return new KeyValueInMemoryRepository;

        case KeyValueRepositoryType::Persisted:
            return new KeyValuePersistedRepository;
    }

    return Q_NULLPTR;
}
