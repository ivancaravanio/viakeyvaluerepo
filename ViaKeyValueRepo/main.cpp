#include "AbstractKeyValueRepository.h"
#include "HttpServer.h"
#include "KeyValueRepositoryConfig.h"
#include "KeyValueRepositoryFactory.h"

#include <QScopedPointer>

int main( int argc, char* argv[] )
{
    QCoreApplication app{ argc, argv };

    const KeyValueRepositoryConfig               config = KeyValueRepositoryConfig::from( app );
    QScopedPointer< AbstractKeyValueRepository > keyValueRepository{
        KeyValueRepositoryFactory::create( config.keyValueRepositoryType.value_or(
            KeyValueRepositoryFactory::KeyValueRepositoryType::Persisted ) )
    };
    HttpServer httpServer{ keyValueRepository.data(),
                           config.host.isNull() ? HttpServer::s_defaultHost : config.host,
                           config.port.value_or( HttpServer::s_defaultPort ) };

    return app.exec();
}
