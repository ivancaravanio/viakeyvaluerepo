#ifndef KEYVALUEPERSISTEDREPOSITORY_H
#define KEYVALUEPERSISTEDREPOSITORY_H

#include "AbstractKeyValueRepository.h"

#include <QHash>
#include <QSqlDatabase>
#include <QString>

class KeyValuePersistedRepository : public AbstractKeyValueRepository
{
public:
    KeyValuePersistedRepository();
    ~KeyValuePersistedRepository() Q_DECL_OVERRIDE;

    bool                      set( const QString& key, const QString& value ) Q_DECL_OVERRIDE;
    bool                      remove( const QString& key ) Q_DECL_OVERRIDE;
    void                      clear() Q_DECL_OVERRIDE;
    QString                   value( const QString& key ) const Q_DECL_OVERRIDE;
    QStringList               keys() const Q_DECL_OVERRIDE;
    QStringList               values() const Q_DECL_OVERRIDE;
    QHash< QString, QString > keyValuePairs() const Q_DECL_OVERRIDE;

private:
    void                createAndConnectToDatabase();
    static QSqlDatabase createDatabaseConnection();
    QStringList         entities( const bool shouldProvideValues, const QString& key = {} ) const;

private:
    QSqlDatabase _databaseConnection;
};

#endif // KEYVALUEPERSISTEDREPOSITORY_H
