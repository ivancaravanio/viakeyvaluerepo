#ifndef VIAKEYVALUEREPOSITORYALLHEADERS_H
#define VIAKEYVALUEREPOSITORYALLHEADERS_H

#if defined __cplusplus
#    include <QCommandLineOption>
#    include <QCommandLineParser>
#    include <QCoreApplication>
#    include <QDir>
#    include <QHash>
#    include <QHostAddress>
#    include <QJsonArray>
#    include <QJsonDocument>
#    include <QJsonObject>
#    include <QScopedPointer>
#    include <QSqlDatabase>
#    include <QSqlError>
#    include <QSqlQuery>
#    include <QStandardPaths>
#    include <QString>
#    include <QStringBuilder>
#    include <QStringList>
#    include <QTextStream>
#    include <QVariant>
#    include <QtHttpServer/QHttpServer>
#    include <qcompilerdetection.h>

#    include <functional>
#    include <optional>
#endif

#endif // VIAKEYVALUEREPOSITORYALLHEADERS_H
