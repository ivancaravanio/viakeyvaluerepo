#ifndef HTTPSERVER_H
#define HTTPSERVER_H

#include "AbstractKeyValueRepository.h"

#include <QHostAddress>
#include <QtHttpServer/QHttpServer>

class HttpServer
{
public:
    static const QHostAddress s_defaultHost;
    static const quint16      s_defaultPort;
    static const quint16      s_anyPort;

public:
    // refer to the passed key-value repository weakly
    explicit HttpServer( AbstractKeyValueRepository* const aKeyValueRepository,
                         const QHostAddress&               aHost = s_defaultHost,
                         const quint16                     aPort = s_defaultPort );

private:
private:
    AbstractKeyValueRepository* _keyValueRepository;
    QHttpServer                 _httpServer;
};

#endif // HTTPSERVER_H
