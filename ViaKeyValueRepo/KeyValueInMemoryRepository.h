#ifndef KEYVALUEINMEMORYREPOSITORY_H
#define KEYVALUEINMEMORYREPOSITORY_H

#include "AbstractKeyValueRepository.h"

#include <QHash>
#include <QString>

class KeyValueInMemoryRepository : public AbstractKeyValueRepository
{
public:
    ~KeyValueInMemoryRepository() Q_DECL_OVERRIDE;

    bool                      set( const QString& key, const QString& value ) Q_DECL_OVERRIDE;
    bool                      remove( const QString& key ) Q_DECL_OVERRIDE;
    void                      clear() Q_DECL_OVERRIDE;
    QString                   value( const QString& key ) const Q_DECL_OVERRIDE;
    QStringList               keys() const Q_DECL_OVERRIDE;
    QStringList               values() const Q_DECL_OVERRIDE;
    QHash< QString, QString > keyValuePairs() const Q_DECL_OVERRIDE;

private:
    QHash< QString, QString > _keyValueRepository;
};

#endif // KEYVALUEINMEMORYREPOSITORY_H
