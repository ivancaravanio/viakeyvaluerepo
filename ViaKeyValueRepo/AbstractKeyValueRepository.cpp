#include "AbstractKeyValueRepository.h"

AbstractKeyValueRepository::AbstractKeyValueRepository()
{
}

AbstractKeyValueRepository::~AbstractKeyValueRepository()
{
}

bool AbstractKeyValueRepository::isValid( const QString& entity, const bool isValue )
{
    return entity.size() <= ( isValue ? 256 : 64 );
}

bool AbstractKeyValueRepository::set( const QString& key, const QString& value )
{
    return isValid( key, false ) && isValid( value, true );
}

bool AbstractKeyValueRepository::remove( const QString& key )
{
    return isValid( key, false );
}

QString AbstractKeyValueRepository::value( const QString& key ) const
{
    return isValid( key, false ) ? "" : QString{};
}
